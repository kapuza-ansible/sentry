# Sentry role
## Install
### Install python (ubuntu)
For use ansible, you need install python2.7 on remote server.
```bash
ansible sentry -m raw -a "apt-get -qq update;\
apt-get -y --no-install-recommends install python2.7 python-apt;"
```

### ansible.cfg example
```bash
cat <<'EOF' > ansible.cfg
[defaults]
hostfile = hosts
remote_user = root
deprecation_warnings = False
roles_path = roles
force_color = 1
retry_files_enabled = False
executable = /bin/bash

[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no
pipelining = True
EOF
```

### download sentry role
```bash
mkdir ./roles
cat <<'EOF' >> ./roles/req.yml

- src: git+https://gitlab.com/kapuza-ansible/sentry.git
  name: sentry
EOF
echo "roles/sentry" >> .gitignore
ansible-galaxy install --force -r ./roles/req.yml
```

## Sentry (ubuntu)
Install and setup sentry server.
```bash
cat << 'EOF' > sentry.yml
---
- hosts:
    sentry
  roles:
    - role: sentry
      sentry_postgres_install: true
      sentry_postgres_user: sentryuser
      sentry_postgres_password: sentry
      sentry_postgres_database: database
      sentry_postgres_host: localhost
      sentry_postgres_port: 5432
      sentry_redis_install: true
      sentry_redis_host: 127.0.0.1
      sentry_redis_port: 6379
      sentry_smtp_host: smtp.superdomain.com
      sentry_smtp_port: 25
      sentry_system_secret_key: "uzyu539q^(wuj5%kkje67a#9d@^jnyoq0!#3i1ie)fck)2wai8"
EOF

ansible-playbook ./sentry.yml
```

## Use
```
# Upgrade
sudo -iu www-data
source /var/www/sentry/bin/activate
sentry --config /var/www/settings upgrade --noinput --lock

# Create User
sudo -iu www-data
source /var/www/sentry/bin/activate
sentry --config /var/www/settings createuser
```
